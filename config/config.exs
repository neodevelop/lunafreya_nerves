# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Customize non-Elixir parts of the firmware.  See
# https://hexdocs.pm/nerves/advanced-configuration.html for details.
config :nerves, :firmware, rootfs_overlay: "rootfs_overlay"

# Use shoehorn to start the main application. See the shoehorn
# docs for separating out critical OTP applications such as those
# involved with firmware updates.
config :shoehorn,
  init: [:nerves_runtime, :nerves_init_gadget],
  app: Mix.Project.config()[:app]

# Import target specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
# Uncomment to use target specific configurations

# import_config "#{Mix.Project.config[:target]}.exs"

config :logger, backends: [RingLogger]

config :nerves_firmware_ssh,
authorized_keys: [
  File.read!(Path.join(System.user_home!, ".ssh/id_rsa.pub"))
]

config :logger, backends: [RingLogger]

config :nerves_init_gadget,
  node_name: :target01,
  mdns_domain: "luna.local",
  address_method: :dhcp,
  ifname: "wlan0"

key_mgmt = System.get_env("NERVES_NETWORK_KEY_MGMT") || "WPA-PSK"

config :nerves_network, :default,
wlan0: [
  ssid: System.get_env("NERVES_NETWORK_SSID"),
  psk: System.get_env("NERVES_NETWORK_PSK"),
  key_mgmt: String.to_atom(key_mgmt)
],
eth0: [
  ipv4_address_method: :dhcp
]

config :slack, api_token: System.get_env("SLACK_TOKEN")
config :raspi3, uploader: Raspi3.S3

config :arc,
  storage: Arc.Storage.S3, # or Arc.Storage.Local
  bucket: System.get_env("AWS_S3_BUCKET") # if using Amazon S3

config :ex_aws,
  access_key_id: [System.get_env("AWS_ACCESS_KEY_ID"), :instance_role],
  secret_access_key: [System.get_env("AWS_SECRET_ACCESS_KEY"), :instance_role]
