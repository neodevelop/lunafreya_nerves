defmodule LunafreyaNerves.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  @target Mix.Project.config()[:target]

  use Application

  def start(_type, _args) do
    opts = [strategy: :one_for_one, name: LunafreyaNerves.Supervisor]
    Supervisor.start_link(children(@target), opts)
  end

  # List all child processes to be supervised
  def children("host") do
    [
      # Starts a worker by calling: LunafreyaNerves.Worker.start_link(arg)
      # {LunafreyaNerves.Worker, arg},
    ]
  end

  def children(_target) do
    camera = Application.get_env(:picam, :camera, Picam.Camera)
    token = Application.get_env(:slack, :api_token)

    [
      camera,
      Raspi3.Luna.EyesServer,
      Plug.Cowboy.child_spec(scheme: :http, plug: MyRouter, options: [port: 4001]),
      # %{
      #   id: Slack.Bot,
      #   name: Raspi3.Slack,
      #   start: {
      #     Slack.Bot, :start_link,
      #     [Raspi3.SlackRtm, [], token, %{name: Raspi3.Slack}]
      #   }
      # },
    ]
  end
end
