defmodule Raspi3.Luna.EyesServer do

  use GenServer
  #require Logger

  @uploader Application.get_env(:raspi3, :uploader)

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  def init(args) do
    {:ok, args}
  end

  def open_the_eyes() do
    GenServer.call(__MODULE__, {:open_the_eyes})
  end

  def handle_call({:open_the_eyes}, _from, state) do
    IO.puts "Opening the eyes!"
    result = Task.start fn ->
      IO.puts "Starting a task"
      see_what_happens()
      IO.puts "Finishing the task"
    end
    {:reply, result, state}
  end

  def see_what_happens() do
    Picam.set_size(640, 480)
    filename = "luna_#{:os.system_time}.jpg"
    base_dir = System.tmp_dir!
    File.write!(Path.join(base_dir, filename), Picam.next_frame)
    upload_file(filename)
  end

  def upload_file(gifname) do
    case @uploader.store(Path.join(System.tmp_dir!, gifname)) do
      {:ok, filename} ->
        url = @uploader.url(filename)
        #send Raspi3.Slack, {:message, "#{url}", "#iot"}
      message ->
        msg = "Can't upload image #{inspect message}"
        #send Raspi3.Slack, {:message, msg, "#iot"}
    end
  end

end
