defmodule LunafreyaNerves do
  @moduledoc """
  Documentation for LunafreyaNerves.
  """

  @doc """
  Hello world.

  ## Examples

      iex> LunafreyaNerves.hello
      :world

  """
  def hello do
    :world
  end
end
